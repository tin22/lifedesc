import atexit
import logging

from django.views.generic import ListView, DetailView
from rest_framework import generics, filters

from .models import Daily, Thought, Tag
from .serializers import DailySerializer, ThoughtSerializer, TagSerializer

logger = logging.getLogger(__name__)
logger.info("Application 'lifedesc' started.")


def at_exit():
    """
    Normal shutdown
    """
    logger.info("Application 'lifedesc' exited normally.")


atexit.register(at_exit)


class DailyLastListView(ListView):
    model = Daily
    ordering = '-date'
    template_name = 'daily_list_view.html'


class DailyDetailView(DetailView):
    model = Daily
    serializer_class = DailySerializer
    context_object_name = 'daily'
    template_name = 'daily_detail_view.html'


class DailyViewSet(generics.ListCreateAPIView):
    queryset = Daily.objects.all()
    serializer_class = DailySerializer


class DailyLastViewSet(generics.ListCreateAPIView):
    queryset = Daily.objects.order_by('-date')[:30]
    serializer_class = DailySerializer


class DailyDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Daily.objects.all()
    serializer_class = DailySerializer
    lookup_field = 'date'


class ThoughtListView(generics.ListCreateAPIView):
    serializer_class = ThoughtSerializer

    def get_queryset(self):
        desired_date = self.request.query_params.get('desired_date', None)
        queryset = Thought.objects.filter(timestamp__date=desired_date).order_by('timestamp')
        return queryset


class ThoughtsSearch(generics.ListAPIView):
    """ Search for thoughts on demand.
    """
    search_fields = ['record']
    filter_backends = (filters.SearchFilter,)
    queryset = Thought.objects.all()
    serializer_class = ThoughtSerializer


class AllTagsList(generics.ListAPIView):
    """ All possible tags.
    """
    queryset = Tag.objects.all()
    serializer_class = TagSerializer


def last(request, number=3):
    print('qqqq')
    queryset = Daily.objects.order_by('-date')[:number]
    return queryset
