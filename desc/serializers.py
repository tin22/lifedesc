import logging
import os

from drf_writable_nested.serializers import WritableNestedModelSerializer
from rest_framework import serializers

from .models import Daily, Thought, Tag


class TagSerializer(WritableNestedModelSerializer):
    class Meta:
        model = Tag
        fields = ('id', 'tag',)
        ordering = ('id',)


class ThoughtSerializer(WritableNestedModelSerializer):
    tags = TagSerializer(many=True, required=False)

    class Meta:
        model = Thought
        fields = ('daily', 'timestamp', 'record', 'tags',)
        ordering = ['-timestamp', ]


class DailySerializer(WritableNestedModelSerializer):
    thoughts = ThoughtSerializer(many=True, required=False)

    class Meta:
        model = Daily
        fields = ('id', 'date', 'weight', 'bmi', 'push_ups', 'squates', 'steps',
                  'sleeptime', 'deep_sleeptime', 'dss', 'sa', 'temperature', 'thoughts')


class DailyChartSerializer(serializers.ModelSerializer):
    class Meta:
        model = Daily
        fields = ('date', 'steps', 'weight', 'squates', 'push_ups', 'sleeptime', 'deep_sleeptime',)
