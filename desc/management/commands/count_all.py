from django.core.management.base import BaseCommand, CommandError
from desc import models


class Command(BaseCommand):
    # Задаём текст помощи, который будет
    # отображён при выполнении команды
    # python manage.py count --help
    help = 'Посчитать число записей в базе.'

    def add_arguments(self, parser):
        # Команда может принимать переменное число аргументов,
        # причём при указании '+', должен быть предоставлен
        # хотя бы один агрумент данного типа, а при указании
        #  '*' может быть не предоставлено ни одного
        parser.add_argument('--remove_addr', type=str, default='')
        parser.add_argument('strings', nargs='*', type=str)

    def handle(self, *args, **options):
        print('options: ', options)
        if options['remove_addr']:
            print("remove_addr='{}'".format(options['remove_addr']))
            return

        for string in options['strings']:
            self.stdout.write(string)
            print('ДА!!!') if string == 'eee' else None
        print("That's all")
        return




